import json
from absa.utils import read_absa_data, convert_absa_data
import bert.tokenization as tokenization
import random


def get_bert_tokens(sent_tokens, tokenizer):
    all_doc_tokens = []
    for (i, token) in enumerate(sent_tokens):
        sub_tokens = tokenizer.tokenize(token)
        for sub_token in sub_tokens:
            all_doc_tokens.append(sub_token)
    return all_doc_tokens


def all_analysis(examples, results):
    # aspect extraction
    print("Aspect Extraction:")
    id = 0
    for example in examples:
        result = results[example.example_id]
        id += 1
        # if id < 50:
        #     continue
        # if id not in [58, 90, 114, 232, 322, 374]:
        #     continue
        # if "http" in " ".join(example.sent_tokens):
        #     continue
        # if set(example.term_texts) != set(result["pred_terms"]):
        #     continue
        # # if len(example.sent_tokens) > 30:
        # #     continue
        # # if len(example.term_texts) <= 1:
        # #     continue
        # pred_polarity_dict = {term: polarity for term, polarity in zip(result["pred_terms"], result["pred_polarities"])}
        # example_polarity_dict = {term: polarity for term, polarity in zip(example.term_texts, example.polarities)}
        # all_correct = True
        # for pred_term in result["pred_terms"]:
        #     if pred_term in example.term_texts:
        #         if pred_polarity_dict[pred_term] != example_polarity_dict[pred_term]:
        #             all_correct = False
        # if not all_correct:
        #     continue

        # if 'neutral' in result["pred_polarities"]:
        #     continue

        if random.random() > 0.02:
            continue

        print(id)
        print(" ".join(example.sent_tokens))
        bert_tokens = get_bert_tokens(example.sent_tokens,
                                      tokenizer=tokenization.FullTokenizer(
                                          "/data/linpq/Word2Vec/bert-base-uncased/vocab.txt",
                                          do_lower_case=True))
        print(bert_tokens)
        print(example.term_texts)
        print(example.polarities)
        print(result["pred_terms"])
        print(result["pred_polarities"])
        # word_start_logits = [(word, round(logit, 5)) for word, logit in zip(bert_tokens, result["start_logits"][1:])]
        # word_end_logits = [(word, round(logit, 5)) for word, logit in zip(bert_tokens, result["end_logits"][1:])]
        # print(result["aspect_num"])
        # print(len(bert_tokens) / 13)
        # print(word_start_logits)
        # print(word_end_logits)
        # print(sorted(word_start_logits, key=lambda x: x[1], reverse=True))
        # print(sorted(word_end_logits, key=lambda x: x[1], reverse=True))
        # print(result["start_logits"])
        # print(result["end_logits"])
        # print(result["pred_polarities"])


def error_analysis(examples, results):
    # aspect extraction
    print("Aspect Extraction:")
    for example in examples:
        result = results[example.example_id]
        wrong_aspects = []
        for pred_term in result["pred_terms"]:
            if pred_term not in example.term_texts:
                wrong_aspects.append(pred_term)
        if set(result["pred_terms"]) != set(example.term_texts) and len(wrong_aspects) != 0:
            print(example.sent_tokens)
            print(example.term_texts)
            print(wrong_aspects)


def metric_analysis(examples, results):
    # aspect num
    correct_num = 0
    for example in examples:
        result = results[example.example_id]
        if len(example.term_texts) == len(result["pred_terms"]):
            correct_num += 1
    print("Aspect Number Prediction: Accuracy {:.4f}".format(correct_num / len(results)))

    # aspect extraction
    correct_num = 0
    total_pred_aspect_num = 0
    total_aspect_num = 0
    for example in examples:
        result = results[example.example_id]
        for pred_term in result["pred_terms"]:
            if pred_term in example.term_texts:
                correct_num += 1
        total_pred_aspect_num += len(result["pred_terms"])
        total_aspect_num += len(example.term_texts)
    ae_precision = correct_num / total_pred_aspect_num
    ae_recall = correct_num / total_aspect_num
    ae_f1 = (2 * ae_precision * ae_recall) / (ae_precision + ae_recall)
    print("Aspect Extraction: Precision {:.4f}; Recall {:.4f}; F1 {:.4f}".format(ae_precision, ae_recall, ae_f1))

    # aspect classification
    correct_num = 0
    total_num = 0
    for example in examples:
        result = results[example.example_id]
        pred_polarity_dict = {term: polarity for term, polarity in zip(result["pred_terms"], result["pred_polarities"])}
        example_polarity_dict = {term: polarity for term, polarity in zip(example.term_texts, example.polarities)}
        for pred_term in result["pred_terms"]:
            if pred_term in example.term_texts:
                total_num += 1
                if pred_polarity_dict[pred_term] == example_polarity_dict[pred_term]:
                    correct_num += 1
    print("Aspect Classification: Accuracy {:.4f}".format(correct_num / total_num))

    # Total
    correct_num = 0
    total_pred_aspect_num = 0
    total_aspect_num = 0
    for example in examples:
        result = results[example.example_id]
        pred_polarity_dict = {term: polarity for term, polarity in zip(result["pred_terms"], result["pred_polarities"])}
        example_polarity_dict = {term: polarity for term, polarity in zip(example.term_texts, example.polarities)}
        for pred_term in result["pred_terms"]:
            if pred_term in example.term_texts and pred_polarity_dict[pred_term] == example_polarity_dict[pred_term]:
                correct_num += 1
        total_pred_aspect_num += len(result["pred_terms"])
        total_aspect_num += len(example.term_texts)
    total_precision = correct_num / total_pred_aspect_num
    total_recall = correct_num / total_aspect_num
    total_f1 = (2 * total_precision * total_recall) / (total_precision + total_recall)
    print("Total: Precision {:.4f}; Recall {:.4f}; F1 {:.4f}".format(total_precision, total_recall, total_f1))


if __name__ == "__main__":
    # input_file = "/data/linpq/SpanABSA/out/interact/56/predictions.json"
    # input_file = "/data/linpq/SpanABSA/out/joint/01/predictions.json"
    # predict_file = "../data/absa/rest_total_test.txt"
    # input_file = "/data/linpq/SpanABSA/out/interact/55/predictions.json"
    # input_file = "/data/linpq/SpanABSA/out/joint/03/predictions.json"
    # predict_file = "../data/absa/laptop14_test.txt"
    input_file = "/data/linpq/SpanABSA/out/joint/08/predictions.json"
    # input_file = "/data/linpq/SpanABSA/out/interact/73/predictions.json"
    predict_file = "../data/absa/twitter3_test.txt"

    dataset = read_absa_data(predict_file)
    examples = convert_absa_data(dataset)
    print(len(dataset))
    print(len(examples))
    results = json.load(open(input_file))

    metric_analysis(examples, results)
    # error_analysis(examples, results)
    all_analysis(examples, results)
