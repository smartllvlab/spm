import random


# def func(l):
#     result = []
#     is_start = False
#     for i, v in enumerate(l):
#         if v > 0.5:
#             if not is_start:
#                 start_index = i
#                 is_start = True
#         if v < 0.5:
#             if is_start:
#                 result.append((start_index, i - 1))
#             is_start = False
#     if is_start:
#         result.append((start_index, len(l) - 1))
#     return result
#
#
# assert func([1, 1, 0]) == [(0, 1)], func([1, 1, 0])
# assert func([1, 1, 0, 1]) == [(0, 1), (3, 3)], func([1, 1, 0, 1])
# assert func([0, 1, 1, 0, 1]) == [(1, 2), (4, 4)], func([1, 1, 0, 1])
# assert func([0, 1, 0, 1]) == [(1, 1), (3, 3)], func([1, 1, 0, 1])

index_and_score = sorted(enumerate([0, 1, 5]), key=lambda x: x[1], reverse=True)
print(index_and_score)
