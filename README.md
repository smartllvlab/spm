## Requirements
- Python 3.6
- [Pytorch 1.1](https://pytorch.org/)
- [Allennlp](https://allennlp.org/)

Download the uncased [BERT-Base](https://drive.google.com/file/d/13I0Gj7v8lYhW5Hwmp5kxm3CTlzWZuok2/view?usp=sharing) model

Run the following commands to set up environments:
```bash
export DATA_DIR=data/absa
export BERT_DIR=bert-base-uncased
```

## Multi-target extractor
Train the multi-target extractor:
```shell
python -m absa.run_interact_span
```
